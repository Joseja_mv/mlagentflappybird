﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class BirdAgent : Agent {

    Vector3 startposition;
    private Animator anim;

   
    Bird bird;
    

    public override void InitializeAgent()
    {
        bird = GetComponent<Bird>();
        anim = GetComponent<Animator>();
        startposition = new Vector2(0, 2.33f);
    }

    
    
    public override void CollectObservations()
    {
        var mlGoalObjects = GameObject.FindGameObjectsWithTag("MLGoalTag");
        Transform[] transformsGoals = mlGoalObjects.Select(y => y.transform).ToArray();
        Transform closestPipe = GetClosestPipe(transformsGoals);

        if (closestPipe != null)
        {
            AddVectorObs(gameObject.transform.position.x - closestPipe.position.x);
            AddVectorObs(gameObject.transform.position.y - closestPipe.position.y);
        }
        else
        {
            AddVectorObs(0);
            AddVectorObs(0);
        }
        AddVectorObs(gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        float reward = 0.1f;
        if(Mathf.FloorToInt(vectorAction[0]) == 1)
        {
            bird.Flapping();
        }

        if (bird.isDead)
        {
            reward = -1f;
            
            Done();
        }
        else
        {
            if (bird.gotScore)
            {
                bird.gotScore = false;
                reward = 0.1f;
                
            }
            //if(transform.position.y <= )
        }
        AddReward(reward);
    }

    Transform GetClosestPipe(Transform[] pipes)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;

        foreach(Transform potentialTarget in pipes)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;

            if(dSqrToTarget < closestDistanceSqr && potentialTarget.position.x > currentPosition.x)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }
        return bestTarget;
    }

    public override void AgentReset()
    {
        
        transform.position = startposition;

        anim.SetTrigger("Idle");

        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        var poolingScript = Object.FindObjectOfType<ColumnPool>();
        poolingScript.ResetPositions();

        ScrollingObject[] scrollingObjects = (ScrollingObject[])GameObject.FindObjectsOfType(typeof(ScrollingObject));
        foreach(var scrollingObject in scrollingObjects)
        {
            scrollingObject.InitScrollingObject();
        }
        bird.isDead = false;
        GameControl.instance.gameOver = false;
    }
    
    public override void AgentOnDone()
    {
        
    }
}
